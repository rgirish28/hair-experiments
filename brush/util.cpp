
#include "util.h"

double proper_mod(double x, double m) {
  double ans = fmod(x, m);
  return ans < 0.0 ? ans + m: ans;
}

int proper_mod(int x, int m) {
  int ans = x % m;
  return ans < 0 ? ans + m: ans;
}

double acos_safe(double x) {
  if (x <= -10) {
    return M_PI;
  } else if (x >= 1.0) {
    return 0.0;
  } else {
    return acos(x);
  }
}

vec3 pseudonormalize(vec3 v) {
  double scale = v.x*v.x + v.y*v.y + v.z*v.z;
  return scale != 0.0 ? v * (1.0/sqrt(scale)): vec3(1.0, 0.0, 0.0);
}

double linearstep(double x, double a, double b) {
  return x < a ? 0.0: x > b ? 1.0: (x-a)/(b-a);
}

Exception::Exception(string msg_) {
  msg = msg_;
}

Exception::~Exception() throw() {
}

const char *Exception::what() const throw() {
  return msg.c_str();
}

OSError::OSError(string msg_) :Exception(msg_) {
}

static string read_file_mode(string filename, string mode) {
  FILE *f = fopen(filename.c_str(), mode.c_str());
  if (!f) {
    throw OSError("Could not open file '" + filename + "'");
  }
  string ans;
  while (true) {
    char c = fgetc(f);
    if (c == EOF) { break; }
    ans.push_back(c);
  }
  return ans;
}

string read_binary_file(string filename) {
  return read_file_mode(filename, "rb");
}

string read_text_file(string filename) {
  return read_file_mode(filename, "rt");
}

void draw_box(vec3 a, vec3 b) {
  glBegin(GL_QUADS);

  int old_seed = rand();
  srand(0);
  for (int t = 0; t <= 1; t++) {
    vec3 c = (t == 0 ? a: b);

    glColor3f(random(),random(),random());
    glVertex3f(a.x, b.y, c.z);
    glVertex3f(b.x, b.y, c.z);
    glVertex3f(b.x, a.y, c.z);
    glVertex3f(a.x, a.y, c.z);

    glColor3f(random(),random(),random());
    glVertex3f(a.x, c.y, b.z);
    glVertex3f(b.x, c.y, b.z);
    glVertex3f(b.x, c.y, a.z);
    glVertex3f(a.x, c.y, a.z);

    glColor3f(random(),random(),random());
    glVertex3f(c.x, a.y, b.z);
    glVertex3f(c.x, b.y, b.z);
    glVertex3f(c.x, b.y, a.z);
    glVertex3f(c.x, a.y, a.z);
  }
  srand(old_seed);
  glEnd();
}

void gl_vertex_vec3(vec3 v) {
  glVertex3f(v.x, v.y, v.z);
}

void draw_cylinder(vec3 a, vec3 b, double r, int sides) {
  int i1, i2;
  vec3 ortho1, ortho2;
  try {
    ortho1 = (b-a).ortho().normalize() * r;
    ortho2 = (b-a).cross(ortho1).normalize() * r;
  } catch (EZeroDivisionError e) {
    return;
  }
  int old_seed = rand();
  srand(0);

  glBegin(GL_QUADS);
  for (i1 = 0; i1 < sides; i1++) {
    i2 = (i1+1) % sides;
    double theta1 = i1 * (2.0 * M_PI) / sides;
    double theta2 = i2 * (2.0 * M_PI) / sides;
    glColor3f(random(),random(),random());
    gl_vertex_vec3(a + ortho1 * cos(theta1) + ortho2 * sin(theta1));
    gl_vertex_vec3(b + ortho1 * cos(theta1) + ortho2 * sin(theta1));
    gl_vertex_vec3(b + ortho1 * cos(theta2) + ortho2 * sin(theta2));
    gl_vertex_vec3(a + ortho1 * cos(theta2) + ortho2 * sin(theta2));
  }
  glEnd();

  srand(old_seed);
}

vector<int> range(int n) {
  vector<int> ans;
  for (int i = 0; i < n; i++) {
    ans.push_back(i);
  }
  return ans;
}

vector<int> range(int a, int b) {
  vector<int> ans;
  for (int i = a; i < b; i++) {
    ans.push_back(i);
  }
  return ans;
}
