
/* ---------------------------------------------------------------
   broadphase.cpp: Broadphase collision detection via AABBs.
   --------------------------------------------------------------- */

#include "global.h"

AABB::AABB(vec3 min_, vec3 max_) :min(min_), max(max_) { }

int operator == (AABB a, AABB b) {
  return a.min == b.min && a.max == b.max;
}

int operator != (AABB a, AABB b) {
  return a.min != b.min || a.max != b.max;
}

int box_box_intersects(AABB a, AABB b) {
  return a.min.x < b.max.x && a.min.y < b.max.y && a.min.z < b.max.z && 
         b.min.x < a.max.x && b.min.y < a.max.y && b.min.z < a.max.z;
}

/* ---------------------------------------------------------------
   Naive algorithm
   --------------------------------------------------------------- */

void broadphase_naive(const vector<AABB> &A, const vector<AABB> &B,
                      vector<pair<int, int> > &ans) {
  for (int i = 0; i < (int) A.size(); i++) {
    for (int j = 0; j < (int) B.size(); j++) {
      if (box_box_intersects(A[i], B[j])) {
        ans.push_back(pair<int, int>(i, j));
      }
    }
  }
}

void broadphase_naive(const vector<AABB> &A, vector<pair<int, int> > &ans) {
  for (int i = 0; i < (int) A.size(); i++) {
    for (int j = i + 1; j < (int) A.size(); j++) {
      if (box_box_intersects(A[i], A[j])) {
        ans.push_back(pair<int, int>(i, j));
      }
    }
  }
}

/* ---------------------------------------------------------------
   1D sort algorithm
   --------------------------------------------------------------- */

/* Sort list of objects (actually, sort indices) by minimum x coord. */
class Sort1DComparator { public:
  const vector<AABB> &L;
  Sort1DComparator(const vector<AABB> &L_) :L(L_) { }
  bool operator() (int i, int j) {
    return L[i].min.x < L[j].min.x;
  }
};

Precollider1D::Precollider1D(const vector<AABB> &L_) :L(L_) {
  indices = new int[L.size()];
  for (unsigned int i = 0; i < L.size(); i++) {
    indices[i] = i;
  }
  sort(indices, indices+L.size(), Sort1DComparator(L));
}

Precollider1D::~Precollider1D() {
  delete[] indices;
}

void add_collision_pairs_1d(vector<pair<int, int> > &ans,
                            const Precollider1D &A, const Precollider1D &B,
                            bool swap_pair, bool allow_equal) {
  /* Each AABB collision must have a_minx <= b_minx or b_minx <= a_minx.
     Thus loop over A in order by a_minx and look at b_minx for
     increasing indices of B from just above this value till b_minx
     is above a_maxx.  The broadphase_1d calling routine then switches
     A and B and calls this process again to handle b_minx <= a_minx. */
  int jmin = 0;
  for (int i = 0; i < (int) A.L.size(); i++) {
    AABB abox = A.L[A.indices[i]];
    double aminx = abox.min.x;
    double amaxx = abox.max.x;
    /* Do linear search forward in B while b_minx < a_minx (or <=). */
    if (allow_equal) {
      while (jmin < (int) B.L.size() && B.L[B.indices[jmin]].min.x <= aminx) {
        jmin++;
      }
    } else {
      while (jmin < (int) B.L.size() && B.L[B.indices[jmin]].min.x < aminx) {
        jmin++;
      }
    }
    if (jmin == (int) B.L.size()) { break; }
    int j;
    double bminx;
    if (swap_pair) {
      for (int j = jmin; j < (int) B.L.size(); j++) {
        AABB bbox = B.L[B.indices[j]];
        bminx = bbox.min.x;
        if (bminx >= amaxx) { break; }
        if (abox.min.y < bbox.max.y && abox.min.z < bbox.max.z && 
            bbox.min.y < abox.max.y && bbox.min.z < abox.max.z) {
          ans.push_back(pair<int, int>(B.indices[j], A.indices[i]));
        }
      }
    } else {
      for (j = jmin; j < (int) B.L.size(); j++) {
        AABB bbox = B.L[B.indices[j]];
        bminx = bbox.min.x;
        if (bminx >= amaxx) { break; }
        if (abox.min.y < bbox.max.y && abox.min.z < bbox.max.z && 
            bbox.min.y < abox.max.y && bbox.min.z < abox.max.z) {
          ans.push_back(pair<int, int>(A.indices[i], B.indices[j]));
        }
      }
    }
  }
}

void broadphase_1d(const Precollider1D &A, const Precollider1D &B,
                   vector<pair<int, int> > &ans) {
  add_collision_pairs_1d(ans, A, B, false, true);
  add_collision_pairs_1d(ans, B, A, true, false);
}

void broadphase_1d(const Precollider1D &A, vector<pair<int, int> > &ans) {
  add_collision_pairs_1d(ans, A, A, false, true);
}

/* ---------------------------------------------------------------
   Octree algorithm
   --------------------------------------------------------------- */

/* We use a naive algorithm which starts with a root box and divides
   it down to a fixed depth -- memory requirements are pow(8, depth).
   I would have tried more advanced techniques such as only dividing
   nodes which have above a certain number of objects, but from
   benchmarks (see test/benchmark_results.txt) it was clear that the
   octree cannot compete speedwise with the 1D sort for any depth
   value (assuming the objects are distributed randomly through space
   and do not all have the same x value).  Thus I abandoned the octree
   approach. */
#define OCTREE_DEPTH   2
#define OCTREE_XMASK   15  /* 00001111, lower x half space. */
#define OCTREE_YMASK   51  /* 00110011, lower y half space. */
#define OCTREE_ZMASK   85  /* 01010101, lower z half space. */

/* Node in octree. */
class PrecolliderOctreeNode { public:
  PrecolliderOctreeNode *child[8];
  vector<int> indices;           /* Objects in this node. */

  PrecolliderOctreeNode::PrecolliderOctreeNode(const vector<AABB> &L,
                     AABB box, const vector<int> &incoming, int depth);
  ~PrecolliderOctreeNode();
};

AABB octree_subbox(AABB box, int j) {
  int xlo = (1<<j)&OCTREE_XMASK;
  int ylo = (1<<j)&OCTREE_YMASK;
  int zlo = (1<<j)&OCTREE_ZMASK;
  AABB ans = box;
  vec3 mid = (box.min + box.max) * 0.5;
  if (xlo) { ans.max.x = mid.x; }
  else     { ans.min.x = mid.x; }
  if (ylo) { ans.max.y = mid.y; }
  else     { ans.min.y = mid.y; }
  if (zlo) { ans.max.z = mid.z; }
  else     { ans.min.z = mid.z; }
  return ans;
}

PrecolliderOctreeNode::PrecolliderOctreeNode(const vector<AABB> &L,
    AABB box, const vector<int> &incoming, int depth) {
  int i, j;
  child[0] = child[1] = child[2] = child[3] =
  child[4] = child[5] = child[6] = child[7] = NULL;

  if (depth >= OCTREE_DEPTH) {
    /* All objects go in current node. */
    indices = incoming;
  } else {
    for (j = 0; j < 8; j++) {
      vector<int> sub;
      AABB sub_box = octree_subbox(box, j);
      for (i = 0; i < (int) incoming.size(); i++) {
        if (box_box_intersects(L[incoming[i]], sub_box)) {
          sub.push_back(incoming[i]);
        }
      }
      if (sub.size() != 0) {
        child[j] = new PrecolliderOctreeNode(L, sub_box, sub, depth+1);
      }
    }
  }
}

PrecolliderOctreeNode::~PrecolliderOctreeNode() {
  for (int i = 0; i < 8; i++) {
    if (child[i] != NULL) {
      delete child[i];
    }
  }
}

PrecolliderOctree::PrecolliderOctree(const vector<AABB> &L_, AABB box_) 
  :L(L_), box(box_) {
  root = new PrecolliderOctreeNode(L, box, range(L.size()), 0);
}

PrecolliderOctree::~PrecolliderOctree() {
  delete root;
}

void octree_recurse_binary(vector<pair<int, int> > &ans, set<pair<int, int> > &visited,
                           const PrecolliderOctree &A, const PrecolliderOctree &B,
                           PrecolliderOctreeNode *a, PrecolliderOctreeNode *b,
                           AABB box) {
  int i, j;
  if (a->indices.size() != 0 || b->indices.size() != 0) {
    for (i = 0; i < (int) a->indices.size(); i++) {
      for (j = 0; j < (int) b->indices.size(); j++) {
        if (box_box_intersects(A.L[a->indices[i]],
                               B.L[b->indices[j]])) {
          pair<int, int> p(a->indices[i], b->indices[j]);
          if (visited.count(p) == 0) {
            ans.push_back(p);
            visited.insert(p);
          }
        }
      }
    }
  } else {
    for (j = 0; j < 8; j++) {
      if (a->child[j] != NULL && b->child[j] != NULL) {
        AABB sub = octree_subbox(box, j);
        octree_recurse_binary(ans, visited, A, B, a->child[j], b->child[j], sub);
      }
    }
  }
}

void broadphase_octree(const PrecolliderOctree &A, const PrecolliderOctree &B,
                       vector<pair<int, int> > &ans) {
  set<pair<int, int> > visited;
  if (A.box != B.box) {
    printf("Octree root boxes differ\n"); exit(1);
  }
  octree_recurse_binary(ans, visited, A, B, A.root, B.root, A.box);
}
