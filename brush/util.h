
/* util.h: Miscellaneous utility functions missing from C++. */

#ifndef _split_h
#define _split_h

#include "global.h"

/* Proper mod: returns least nonnegative z such that z == x (mod m), assuming m > 0. */
double proper_mod(double x, double m);
int proper_mod(int x, int m);

/* Arccosine with argument clamped to [-1, 1]. */
double acos_safe(double x);

/* Read binary contents of file as string.  May throw OSError. */
string read_binary_file(string filename);

/* Read text contents of file as string.  May throw OSError. */
string read_text_file(string filename);

/* Draw axis aligned box between two vertices. */
void draw_box(vec3 a=vec3(-1,-1,-1), vec3 b=vec3(1,1,1));

/* Draw cylinder between two vertices. */
void draw_cylinder(vec3 a=vec3(0,0,0), vec3 b=vec3(0,0,1), double r=1.0, int sides=8);

/* Execute glVertex3f() with given floating point vertex. */
void gl_vertex_vec3(vec3 v);

/* Return normalized copy of vector v, or (1, 0, 0) if v is zero */
vec3 pseudonormalize(vec3 v);

/* Like GLSL smoothstep(), but linear in [a, b]. */
double linearstep(double x, double a, double b);

/* Return a if t = 0, b if t = 1, linear blend otherwise. */
template<class T>
T mix(T a, T b, double t) {
  return a + (b - a) * t;
}

/* Delete every pointer in a list. */
template<class T>
void delete_pointer_list(vector<T> L) {
  for (int i = 0; i < (int) L.size(); i++) {
    delete L[i];
  }
}

/* Return copy of x clamped to [a, b]. */
template<class T>
T clamp(T x, T a, T b) {
  return min(max(x, a), b);
}

/* Convert to string. */
template<class T>
string to_string(T x) {
  ostringstream buf;
  buf << x;
  return buf.str();
}

/* Error in OS, e.g. filesystem error. */
class Exception: public exception { public:
  string msg;

  Exception(string msg_);
  ~Exception() throw();

  const char* what() const throw();
};

class OSError: public Exception { public:
  OSError(string msg_);
};

/* Same as Python's range(n)=[0,1,...,n-1] or range(a,b)=[a,a+1,...,b-1]. */
vector<int> range(int n);
vector<int> range(int a, int b);

#endif
