
/* Brush simulation.
   Girish Ramesh, 2010. 

   Controls:
     Arrow keys      - Move camera (hold Alt to sidestep).
     Spacebar        - Switch between scenes.
     G               - Hold to remove gravity.
     B               - Hold to bend brush backwards. */

#include "global.h"
#include "constraint.h"

void create_new_objects();

/* Move line segment a-b outside sphere at given center and radius. */
void move_segment_outside_sphere(vec3 &a, vec3 &b, vec3 center, double radius) {
  vec3 a0 = a - center, b0 = b - center;
  vec3 v = b0-a0;
  double t = -(a0*v)/v.length();
  t = clamp(t, 0.0, 1.0);
  vec3 p = a0 + v*t;
  if (p.length() < radius) {
    vec3 displace = p.normalize() * radius - p;
    a += displace;
    b += displace;
  }
}

/* A sphere which moves to the right. */
class Sphere: public Object { public:
  vec3 initial_pos;
  vec3 pos;
  vec3 vel;
  double radius;
  polymesh mesh;
  Sphere(vec3 initial_pos_=vec3(-2.0, 0.0, -0.8)) {
    initial_pos = initial_pos_;
    pos = initial_pos;
    vel = -vec3(initial_pos.x, initial_pos.y, 0.0).normalize() * 0.3;
    radius = 0.5;
    mesh = polymesh_sphere(vec3(), radius);
  }

  virtual void update() {
    /* Update sphere position. */
    pos += vel * world.dt;
    if (pos.length() > initial_pos.length() || key[KEY_R]) {
      pos = initial_pos;
    }
  }

  virtual void render() {
    glEnable(GL_DEPTH_TEST);
    glPushMatrix();
    glTranslatef(pos.x, pos.y, pos.z);
    render_polymesh(mesh);
    glPopMatrix();
  }
};

Sphere *sphere = NULL;

/* Helper class, initializes the scene when user pressed spacebar. */
class NewSceneCreator: public Object { public:
  int current_key_space;
  NewSceneCreator() {
    current_key_space = 0;
  }
  virtual void update() {
    int prev_key_space = current_key_space;
    current_key_space = key[KEY_SPACE];
    if (current_key_space && !prev_key_space) {
      create_new_objects();
    }
  }
};

/* A single strand of hair. */
class Hair: public Object { public:
  vector<vec3> old_pos; /* Previous joint positions for Verlet integrator. */
  vector<vec3> pos;     /* Current joint positions for Verlet integrator. */
  vector<vec3> accel;   /* Acceleration of each joint. */
  vector<double> link_length;  /* Desired length between successive joints. */
  vec3 g;                      /* Acceleration due to gravity. */
  double total_length;         /* Sum of all link lengths. */
  int max_constrain_iters;     /* Number of relaxation iterations. */

  Hair(vector<vec3> pos_, vec3 g_=vec3(0,0,-9.8), int max_constrain_iters_=20) {
    max_constrain_iters = max_constrain_iters_;
    total_length = 0.0;
    for (int i = 0; i < (int) pos_.size(); i++) {
      pos.push_back(pos_[i]);
      old_pos.push_back(pos_[i]);
      accel.push_back(vec3());
      if (i < (int) (pos_.size()-1)) {
        double d = (pos_[i+1]-pos_[i]).length();
        link_length.push_back(d);
        total_length += d;
      }
    }
    g = g_;
  }

  /* Get Axis-Aligned Bounding Box (AABB) for entire hair. */
  AABB get_box() {
    AABB ans(pos[0], pos[0]);
    for (int i = 1; i < (int) pos.size(); i++) {
      vec3 p = pos[i];
      if (p.x < ans.min.x) { ans.min.x = p.x; }
      if (p.y < ans.min.y) { ans.min.y = p.y; }
      if (p.z < ans.min.z) { ans.min.z = p.z; }
      if (p.x > ans.max.x) { ans.max.x = p.x; }
      if (p.y > ans.max.y) { ans.max.y = p.y; }
      if (p.z > ans.max.z) { ans.max.z = p.z; }
    }
    return ans;
  }

  void constrain() {
    int i, k;
    for (k = 0; k < max_constrain_iters; k++) {
      /* Adjust segment lengths. */
      CONSTRAIN_FIX_MOVE(pos[0], pos[1], link_length[0]);
      for (i = 1; i < (int) (pos.size() - 1); i++) {
        CONSTRAIN_MOVE_MOVE(pos[i], pos[i+1], link_length[i]);
      }
      pos[0] = old_pos[0];

      if ((k & 15) == 0) {
        /* Move segments outside of sphere. */
        double min_dist2 = 1e100;
        for (i = 0; i < (int) pos.size(); i++) {
          vec3 delta = pos[i] - sphere->pos;
          double dist2 = delta.x * delta.x + delta.y * delta.y + delta.z * delta.z;
          if (dist2 < min_dist2) { min_dist2 = dist2; }
        }

        if (sqrt(min_dist2) < sphere->radius + link_length[0]) {
          for (i = 1; i < (int) (pos.size() - 1); i++) {
            move_segment_outside_sphere(pos[i], pos[i+1], sphere->pos, sphere->radius);
          }
        }
      }
    }
  }

  virtual void update() {
    int i;
    double dt = world.dt;

    /* Update positions. */
    vector<vec3> new_pos;
    for (i = 0; i < (int) pos.size(); i++) {
      vec3 new_pos = 2.0*pos[i]-old_pos[i]+accel[i]*(dt*dt);
      old_pos[i] = pos[i];
      pos[i] = new_pos;
    }

    constrain();

    /* Reset forces to force of gravity. */
    vec3 base_gravity = key[KEY_G] ? 0.0*g: g;
    vec3 base_accel = key[KEY_B] ? base_gravity+vec3(0,9.8,0): base_gravity;
    for (i = 0; i < (int) pos.size(); i++) {
      accel[i] = base_accel;
    }

    /* Apply drag forces. */
    double kdrag = 0.5;
    for (i = 1; i < (int) pos.size(); i++) {
      accel[i] -= kdrag * (pos[i]-old_pos[i]) / dt;
    }
  }

  virtual void render() {
    double scale = 0.5;
    double cyl_radius = 0.025*scale;
    glEnable(GL_DEPTH_TEST);
    for (int i = 0; i < (int) pos.size(); i++) {
      if (i < (int) (pos.size()-1)) {
        draw_cylinder(pos[i], pos[i+1], cyl_radius);
      }
    }
  }
};

/* Make linearly interpolated list of positions for Hair() constructor. */
vector<vec3> make_linear_pos_list(vec3 a, vec3 b, int n) {
  vector<vec3> ans;
  for (int i = 0; i < n; i++) {
    double t = i / (n - 1.0);
    ans.push_back(a+t*(b-a));
  }
  return ans;
}

/* Collision response for hair segment - hair segment collision. */
void segment_segment_collide(Hair &a, Hair &b, int i, int j, double d, double hair_radius) {
  double dist = (2.0 * hair_radius - d) / (2.0 * hair_radius);
  vec3 pa = a.pos[i] + a.pos[i+1];
  vec3 pb = b.pos[j] + b.pos[j+1];
  vec3 v = (pb - pa).normalize();
  double Fmag = 10.0;
  vec3 F = (Fmag * dist) * v;
  a.accel[i] -= F;
  a.accel[i+1] -= F;
  b.accel[j] += F;
  b.accel[j+1] += F;
}

/* Collision response for hair - hair collision. */
void hair_hair_collide(Hair &a, Hair &b, double hair_radius) {
  double d2 = 4.0 * hair_radius * hair_radius;
  for (int i = 0; i < (int) (a.pos.size() - 1); i++) {
    for (int j = 0; j < (int) (b.pos.size() - 1); j++) {
      vec3 delta = a.pos[i] - b.pos[j];
      double dsquared = delta.x*delta.x+delta.y*delta.y+delta.z*delta.z;
      if (dsquared < d2) {
        segment_segment_collide(a, b, i, j, sqrt(dsquared), hair_radius);
      }
    }
  }
}

/* Rectangular grid of hairs. */
class HairGrid: public Object { public:
  vector<vector<Hair *> > hairs;
  int nx, ny;
  double w, h;
  HairGrid(int nx_=12, int ny_=6, double w_=1.0, double h_=0.5, double hairlen=1.0, int nodes=20,
           vec3 g = vec3(0,0,-9.8), int max_constrain_iters=40) {
    nx = nx_;
    ny = ny_;
    w = w_;
    h = h_;
    for (int i = 0; i < ny; i++) {
      double y = (-0.5 + i*1.0/ny) * h;
      vector<Hair *> row;
      for (int j = 0; j < nx; j++) {
        double x = (-0.5 + j*1.0/nx) * w;
        row.push_back(new Hair(make_linear_pos_list(vec3(x,y,0), vec3(x,y,-1)*hairlen, nodes), g, max_constrain_iters));
      }
      hairs.push_back(row);
    }
  }

  virtual ~HairGrid() {
    for (int i = 0; i < ny; i++) {
      for (int j = 0; j < nx; j++) {
        delete hairs[i][j];
      }
    }
  }

  virtual void update() {
    /* Bounding boxes for entire hairs. */
    vector<AABB> hair_box;

    double hair_radius = (0.5*h/ny) * 0.9;

    int i, j;
    for (i = 0; i < ny; i++) {
      for (j = 0; j < nx; j++) {
        /* Hair update. */
        hairs[i][j]->update();

        /* Hair-hair interactions: add hair strand AABB to list. */
        AABB box = hairs[i][j]->get_box();
        box.min.x -= hair_radius;
        box.min.y -= hair_radius;
        box.min.z -= hair_radius;
        box.max.x += hair_radius;
        box.max.y += hair_radius;
        box.max.z += hair_radius;
        hair_box.push_back(box);
      }
    }

    /* Collide bounding boxes with broadphase 1D sort algorithm. */
    Precollider1D pre(hair_box);
    vector<pair<int, int> > pairs;
    broadphase_1d(pre, pairs);

    /* Respond to hair-hair segment collisions. */
    for (i = 0; i < (int) pairs.size(); i++) {
      int k1 = pairs[i].first, k2 = pairs[i].second;
      int i1 = k1/nx, j1 = k1%nx;
      int i2 = k2/nx, j2 = k2%nx;
      hair_hair_collide(*hairs[i1][j1], *hairs[i2][j2], hair_radius);
    }
  }

  virtual void render() {
    for (int i = 0; i < ny; i++) {
      for (int j = 0; j < nx; j++) {
        hairs[i][j]->render();
      }
    }
  }
};

HairGrid *grid = NULL;

/* Create new global object variables. */
int global_counter = 0;
void create_new_objects() {
  if (grid != NULL) {
    grid->remove();
  }
  if (sphere != NULL) {
    sphere->remove();
  }

  if (global_counter % 2 == 0) {
    grid = new HairGrid();
  } else {
    grid = new HairGrid(3, 3, 0.1, 0.1, 3.0, 50, vec3(0,0,-9.8), 120);
  }

  sphere = new Sphere();
  world.add(sphere);

  world.add(grid);
  global_counter++;
}

void init_scene(int argc, char *argv[]) {
  create_new_objects();
  world.add(new Camera());
  world.add(new BeginScene());
  world.add(new EndScene());
  world.add(new NewSceneCreator());
}

void directions() {
  printf("Brush simulation.\n"
         "Connelly Barnes, 2006.\n\n"
         "Controls:\n"
         "  Arrow keys      - Move camera (hold Alt to sidestep).\n"
         "  Spacebar        - Switch between scenes.\n"
         "  G               - Hold to remove gravity.\n"
         "  B               - Hold to bend brush backwards.\n");
}

int main(int argc, char *argv[]) {
  directions();
  alutil_init_gfx_mode();
  alutil_init_perspective();

  init_scene(argc, argv);

  /* Main loop. */
  while (!key[KEY_ESC]) {
    world.update();
    world.render();
  }

  return 0;
}

/* Allegro preprocessor magic to make platform specific main function. */
END_OF_MAIN();
