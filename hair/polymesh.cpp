
/* ----------------------------------------------------------------
   polymesh.cpp: Simple operations on lists of polygons.
   ---------------------------------------------------------------- */

#include "polymesh.h"

polymesh transformed_polymesh(polymesh mesh, mat4 A) {
  polymesh ans;
  for (int i = 0; i < (int) mesh.size(); i++) {
    vector<vec3> poly;
    for (int j = 0; j < (int) mesh[i].size(); j++) {
      poly.push_back(A * mesh[i][j]);
    }
    ans.push_back(poly);
  }
  return ans;
}

polymesh polymesh_pyramid(int nsides, double height, double radius) {
  polymesh ans;
  vector<vec3> poly;
  int i;
  for (i = 0; i < nsides; i++) {
    double t = i*(2.0*M_PI/nsides);
    poly.push_back(vec3(cos(t), sin(t), 0)*radius);
  }
  ans.push_back(poly);
  for (i = 0; i < nsides; i++) {
    int j = (i+1)%nsides;
    double ti = i*(2.0*M_PI/nsides);
    double tj = j*(2.0*M_PI/nsides);
    poly.clear();
    poly.push_back(vec3(cos(ti), sin(ti), 0)*radius);
    poly.push_back(vec3(cos(tj), sin(tj), 0)*radius);
    poly.push_back(vec3(0, 0, height));
    ans.push_back(poly);
  }
  return ans;
}

polymesh polymesh_cylinder(int nsides, double height, double radius) {
  polymesh ans;
  vector<vec3> poly;
  int i, j;
  for (j = 0; j < 2; j++) {
    poly.clear();
    for (i = 0; i < nsides; i++) {
      double t = i*(2.0*M_PI/nsides);
      poly.push_back(vec3(cos(t), sin(t), j*height));
    }
    ans.push_back(poly);
  }
  for (i = 0; i < nsides; i++) {
    int j = (i+1)%nsides;
    double ti = i*(2.0*M_PI/nsides);
    double tj = j*(2.0*M_PI/nsides);
    poly.clear();
    poly.push_back(vec3(cos(ti), sin(ti), 0));
    poly.push_back(vec3(cos(ti), sin(ti), height));
    poly.push_back(vec3(cos(tj), sin(tj), height));
    poly.push_back(vec3(cos(tj), sin(tj), 0));
    ans.push_back(poly);
  }
  return ans;
}

polymesh polymesh_torus(int ntheta, int nsmall, double rmain, double rsmall, bool slice, double start_angle, double end_angle) {
  polymesh ans;
  for (int i = 0; i < ntheta; i++) {
    int i2 = slice ? i+1: (i+1)%ntheta;
    double theta1 = start_angle+(end_angle-start_angle)*i/ntheta;
    double theta2 = start_angle+(end_angle-start_angle)*i2/ntheta;
    vec3 out1 = vec3(cos(theta1), sin(theta1), 0);
    vec3 out2 = vec3(cos(theta2), sin(theta2), 0);
    vec3 up = vec3(0, 0, 1);
    for (int j = 0; j < nsmall; j++) {
      vector<vec3> poly;
      int j2 = (j+1)%nsmall;
      double phi1 = 2.0*M_PI*j/nsmall;
      double phi2 = 2.0*M_PI*j2/nsmall;
      vec3 out11 = out1 * cos(phi1) + up * sin(phi1);
      vec3 out12 = out1 * cos(phi2) + up * sin(phi2);
      vec3 out21 = out2 * cos(phi1) + up * sin(phi1);
      vec3 out22 = out2 * cos(phi2) + up * sin(phi2);
      poly.push_back(out1*rmain+out11*rsmall);
      poly.push_back(out1*rmain+out12*rsmall);
      poly.push_back(out2*rmain+out22*rsmall);
      poly.push_back(out2*rmain+out21*rsmall);
      ans.push_back(poly);
    }
  }
  return ans;
}

polymesh polymesh_sphere(vec3 center, double r, int nphi, int stacks) {
  polymesh ans;
  for (int i = 0; i < stacks; i++) {
    double theta1 = i*M_PI/stacks;
    double theta2 = (i+1)*M_PI/stacks;
    for (int j = 0; j < nphi; j++) {
      vector<vec3> poly;
      double phi1 = j*2.0*M_PI/nphi;
      double phi2 = ((j+1)%nphi)*2.0*M_PI/nphi;
      vec3 p1 = center + r*vec3(cos(phi1)*sin(theta1), sin(phi1)*sin(theta1), cos(theta1));
      vec3 p2 = center + r*vec3(cos(phi2)*sin(theta1), sin(phi2)*sin(theta1), cos(theta1));
      vec3 p3 = center + r*vec3(cos(phi2)*sin(theta2), sin(phi2)*sin(theta2), cos(theta2));
      vec3 p4 = center + r*vec3(cos(phi1)*sin(theta2), sin(phi1)*sin(theta2), cos(theta2));
      if (i == 0) {
        poly.push_back(p1);
        poly.push_back(p3);
        poly.push_back(p4);
      } else if (i == nphi - 1) {
        poly.push_back(p1);
        poly.push_back(p2);
        poly.push_back(p3);
      } else {
        poly.push_back(p1);
        poly.push_back(p2);
        poly.push_back(p3);
        poly.push_back(p4);
      }
      ans.push_back(poly);
    }
  }
  return ans;
}

void render_polymesh(polymesh &mesh) {
  glDisable(GL_CULL_FACE);
  glEnable(GL_DEPTH_TEST);
  glDisable(GL_TEXTURE_2D);
  glDisable(GL_BLEND);
  glShadeModel(GL_SMOOTH);
  glDepthMask(GL_TRUE);

  int old_seed = rand();
  srand(0);
  for (int i = 0; i < (int) mesh.size(); i++) {
    vector<vec3> poly = mesh[i];
    glBegin(GL_POLYGON);
    for (int j = 0; j < (int) poly.size(); j++) {
      double r = random();
      glColor3f(r, r, r);
      glVertex3f(poly[j].x, poly[j].y, poly[j].z);
    }
    glEnd();
  }
  srand(old_seed);
}

void polymesh_get_bounding_box(polymesh &mesh, vec3 *box_min, vec3 *box_max) {
  *box_min = *box_max = mesh[0][0];
  for (int i = 0; i < (int) mesh.size(); i++) {
    vector<vec3> poly = mesh[i];
    for (int j = 0; j < (int) poly.size(); j++) {
      if (poly[j].x < box_min->x) { box_min->x = poly[j].x; }
      if (poly[j].y < box_min->y) { box_min->y = poly[j].y; }
      if (poly[j].z < box_min->z) { box_min->z = poly[j].z; }
      if (poly[j].x > box_max->x) { box_max->x = poly[j].x; }
      if (poly[j].y > box_max->y) { box_max->y = poly[j].y; }
      if (poly[j].z > box_max->z) { box_max->z = poly[j].z; }
    }
  }
}
