
/* ---------------------------------------------------------------
   global.h: Include useful globals.
   --------------------------------------------------------------- */

#ifndef _global_h
#define _global_h

/* ---------------------------------------------------------------
   Includes
   --------------------------------------------------------------- */

#include <allegro.h>
#include <alleggl.h>
#include <stdlib.h>

#include "cgkit/math3d.h"

#ifndef M_PI
#define M_PI 3.14159265358979323846264338327950288419716939937510
#endif

/* Uniform random number in [0.0, 1.0). */
#define random() ((rand()^rand()) / (RAND_MAX+1.0))

/* Uniform random number in [-1.0, 1.0) (mnemonic: "centered random"). */
#define crandom() (2.0*random()-1.0)

/* Loop over STL iterator less painfully.  Here "it" is the "iterator" that
   will move through L, L is the collection, and type is the type of L. */
#define for_each(type, it, L) for (type::iterator (it) = (L).begin(); (it) != (L).end(); (it)++)

using namespace std;

/* ---------------------------------------------------------------
   Primitive data types
   --------------------------------------------------------------- */

typedef unsigned char uint8;
typedef unsigned short uint16;
typedef unsigned int uint32;

typedef char int8;
typedef short int16;
typedef int int32;

/* ---------------------------------------------------------------
   Collections
   --------------------------------------------------------------- */

#include <algorithm>
#include <sstream>
#include <vector>
#include <string>
#include <set>
#include <map>

/* Geometric polygon soup without any normals, texture coords, etc. */
typedef vector<vector<vec3> > polymesh;

/* ---------------------------------------------------------------
   Project Specific Includes
   --------------------------------------------------------------- */

#include "util.h"
#include "alutil.h"
#include "object.h"
#include "polymesh.h"
#include "broadphase.h"

#ifndef min
#undef min
#endif

#ifndef max
#undef max
#endif

#endif
