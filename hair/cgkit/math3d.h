
/* math3d.h: 3D math routines from LGPL open source library CGKIT.
             See http://cgkit.sourceforge.net/ . */

#ifndef _math3d_h
#define _math3d_h

#include "mat3.h"
#include "mat4.h"
#include "vec3.h"
#include "vec4.h"
#include "common_exceptions.h"

typedef support3d::mat3<double> mat3;
typedef support3d::mat4<double> mat4;
typedef support3d::vec3<double> vec3;
typedef support3d::vec4<double> vec4;
typedef support3d::EZeroDivisionError EZeroDivisionError;

#endif
