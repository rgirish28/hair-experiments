
/* ----------------------------------------------------------------
   polymesh.h: Simple operations on lists of polygons.
   ---------------------------------------------------------------- */

#ifndef _polymesh_h
#define _polymesh_h

#include "global.h"

/* Get copy of given polymesh pre-multiplied by matrix A. */
polymesh transformed_polymesh(polymesh mesh, mat4 A);

/* Pyramid, centered at zero, with apex pointing in +z direction. */
polymesh polymesh_pyramid(int nsides=4, double height=1.0, double radius=1.0);

/* Cylinder, lower circle centered at zero, pointing in +z direction. */
polymesh polymesh_cylinder(int nsides=4, double height=1.0, double radius=1.0);

/* Sphere, centered at center, radius r. */
polymesh polymesh_sphere(vec3 center=vec3(), double r=1.0, int nphi=32, int stacks=16);

/* Torus, axis of symmetry is +z. */
polymesh polymesh_torus(int ntheta=16, int nsmall=8, double rmain=1.0, double rsmall=0.1, bool slice=false, double start_angle=0, double end_angle=2.0*M_PI);

/* Render the given polygon mesh using flat shading. */
void render_polymesh(polymesh &mesh);

/* Get the minimal AABB for the given polymesh. */
void polymesh_get_bounding_box(polymesh &mesh, vec3 *box_min, vec3 *box_max);

#endif
