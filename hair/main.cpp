
/* Hair simulation.
   Girish Ramesh 2010. 

   Controls:
     Arrow keys       - Move camera in XY plane (hold Alt to sidestep).
     Pageup/Pagedown  - Move up/down.
     G                - Turn off gravity.
     Y                - Nod "yes."
     N                - Shake head "no."
     W                - Fake wind effect.
     M                - View coarse hair mesh.
     Spacebar         - Reset hair.
*/

#include "global.h"
#include "constraint.h"

void create_new_objects();

/* Move line segment a-b outside sphere at given center and radius. */
void move_segment_outside_sphere(vec3 &a, vec3 &b, vec3 center, double radius) {
  /* Find closest point on line segment to the sphere's center. */
  vec3 a0 = a - center, b0 = b - center;
  vec3 v = b0-a0;
  double t = -(a0*v)/v.length();
  t = clamp(t, 0.0, 1.0);
  vec3 p = a0 + v*t;
  /* Displace that point out from center of sphere, if it is too close.
     Make the segment endpoints undergo the same displacement. */
  if (p.length() < radius) {
    vec3 displace = p.normalize() * radius - p;
    a += displace;
    b += displace;
  }
}

/* A sphere, used for a head. */
class Sphere: public Object { public:
  vec3 initial_pos;
  vec3 pos;
  double radius;
  polymesh mesh;
  Sphere(vec3 pos_=vec3()) {
    pos = pos;
    radius = 0.5;
    mesh = polymesh_sphere(vec3(), radius);
  }
  virtual void render();
};

Sphere *sphere = NULL;

/* A single strand of hair.

   The hair is represented by a number of joints.  Successive joints
   are stored in the 'pos' list and rendered as connected with lines.
   It is assumed that the distance between successive joints is
   constant across the entire hair (the lengths may change due to
   constraints not being fully satisfied, but we do attempt to keep the
   lengths constant). */
class Hair: public Object { public:

  /* Previous position of hair joints, used by Verlet integrator. */
  vector<vec3> old_pos;

  /* Current position of hair joints, used by Verlet integrator. */
  vector<vec3> pos;

  /* Current acceleration of hair joints. */
  vector<vec3> accel;

  /* link_length[i] is the desired length between pos[i] and pos[i+1]. */
  vector<double> link_length;

  vec3 g;                   /* Acceleration due to gravity. */
  double total_length;      /* Sum of all link lengths. */
  int max_constrain_iters;  /* Number of times to run constraint relaxation. */

  /* Create a new hair with the given list of joint positions, acceleration
     due to gravity, and number of times to run constraint relaxation. */
  Hair(vector<vec3> pos_, vec3 g_=vec3(0,0,-9.8), int max_constrain_iters_=10) {
    max_constrain_iters = max_constrain_iters_;
    total_length = 0.0;
    for (int i = 0; i < (int) pos_.size(); i++) {
      pos.push_back(pos_[i]);
      old_pos.push_back(pos_[i]);
      accel.push_back(vec3());
      if (i < (int) (pos_.size()-1)) {
        double d = (pos_[i+1]-pos_[i]).length();
        link_length.push_back(d);
        total_length += d;
      }
    }
    g = g_;
  }

  /* Get an Axis-Aligned Bounding Box (AABB) for the entire hair strand. */
  AABB get_box() {
    AABB ans(pos[0], pos[0]);
    for (int i = 1; i < (int) pos.size(); i++) {
      vec3 p = pos[i];
      if (p.x < ans.min.x) { ans.min.x = p.x; }
      if (p.y < ans.min.y) { ans.min.y = p.y; }
      if (p.z < ans.min.z) { ans.min.z = p.z; }
      if (p.x > ans.max.x) { ans.max.x = p.x; }
      if (p.y > ans.max.y) { ans.max.y = p.y; }
      if (p.z > ans.max.z) { ans.max.z = p.z; }
    }
    return ans;
  }

  /* Enforce position constraints.  Velocities need not be computed,
     because we're using Verlet integration (the velocity is
     computed by the integrator from the old and current position). */
  void constrain() {
    int i, k;
    for (k = 0; k < max_constrain_iters; k++) {
      /* Adjust segment lengths. */
      CONSTRAIN_FIX_MOVE(pos[0], pos[1], link_length[0]);
      for (i = 1; i < (int) (pos.size() - 1); i++) {
        CONSTRAIN_MOVE_MOVE(pos[i], pos[i+1], link_length[i]);
      }
      pos[0] = old_pos[0];

      /* Move segments outside of sphere. */
      for (i = 1; i < (int) (pos.size() - 1); i++) {
        move_segment_outside_sphere(pos[i], pos[i+1], sphere->pos, sphere->radius);
      }
    }
  }

  /* Update using Verlet integration. */
  virtual void update() {
    int i;
    double dt = world.dt;

    /* Update positions. */
    vector<vec3> new_pos;
    for (i = 0; i < (int) pos.size(); i++) {
      vec3 new_pos = 2.0*pos[i]-old_pos[i]+accel[i]*(dt*dt);
      old_pos[i] = pos[i];
      pos[i] = new_pos;
    }

    constrain();

    /* Reset forces to force of gravity. */
    vec3 base_gravity = key[KEY_G] ? 0.0*g: g;
    for (i = 0; i < (int) pos.size(); i++) {
      /* Add some wind based on cos() and sin() functions if W key pressed. */
      accel[i] = base_gravity + (key[KEY_W] ? vec3(cos(pos[i].x*5+pos[i].y*7.86+world.t*3*4),sin(pos[i].y*9.13342+pos[i].x*6.23493+world.t*4.34993*4),0.0)*5.0+vec3(-10,-5,0): vec3());
    }

    /* Apply viscous drag forces, assuming "air" is stationary. */
    double kdrag = 0.5;
    for (i = 1; i < (int) pos.size(); i++) {
      accel[i] -= kdrag * (pos[i]-old_pos[i]) / dt;
    }
  }

  /* Render using a green and purple cylinder. */
  virtual void render() {
    double scale = 0.5;
    double cyl_radius = 0.025*scale;
    glEnable(GL_DEPTH_TEST);
    for (int i = 0; i < (int) pos.size(); i++) {
      if (i < (int) (pos.size()-1)) {
        draw_cylinder(pos[i], pos[i+1], cyl_radius);
      }
    }
  }
};

/* Make linearly interpolated list of positions for Hair() constructor. */
vector<vec3> make_linear_pos_list(vec3 a, vec3 b, int n) {
  vector<vec3> ans;
  for (int i = 0; i < n; i++) {
    double t = i / (n - 1.0);
    ans.push_back(a+t*(b-a));
  }
  return ans;
}

/* Collision response function for two hair segments colliding.
   Here a and b are the two hairs, and a.pos[i]-a.pos[i+1] and
   b.pos[j]-b.pos[j+1] are the two hair segments. */
void segment_segment_collide(Hair &a, Hair &b, int i, int j, double hair_radius) {
  /* We need the shortest distance between the two line segments.
     The true shortest distance can be found by calculus: differentiate
     the squared distance as a function of the parameter on the first
     segment u and the parameter on the second segment v.  This results
     in a complicated 2x2 linear system.  I tried this calculus
     approach and it was way too slow.

     Instead, we divide each hair segment in 3 parts.  We then find
     shortest distance between the 3 points on segment a and the 3
     points on segment b.  Alternatively, we could just use the
     midpoint and increase the number of segments per hair; however
     that slows down the whole collision detection phase. */
  vec3 a1 = a.pos[i], a3 = a.pos[i+1];
  vec3 b1 = b.pos[j], b3 = b.pos[j+1];
  vec3 a2 = (a1+a3)*0.5, b2 = (b1+b3)*0.5;
  double d11 = lengthSquared(a1-b1);
  double d12 = lengthSquared(a1-b2);
  double d13 = lengthSquared(a1-b3);
  double d21 = lengthSquared(a2-b1);
  double d22 = lengthSquared(a2-b2);
  double d23 = lengthSquared(a2-b3);
  double d31 = lengthSquared(a3-b1);
  double d32 = lengthSquared(a3-b2);
  double d33 = lengthSquared(a3-b3);
  double d = d11;
  vec3 pa = a1, pb = b1;
  if (d12 < d) { d = d12; pa=a1; pb=b2; }
  if (d13 < d) { d = d13; pa=a1; pb=b3; }
  if (d21 < d) { d = d21; pa=a2; pb=b1; }
  if (d22 < d) { d = d22; pa=a2; pb=b2; }
  if (d23 < d) { d = d23; pa=a2; pb=b3; }
  if (d31 < d) { d = d31; pa=a3; pb=b1; }
  if (d32 < d) { d = d32; pa=a3; pb=b2; }
  if (d33 < d) { d = d33; pa=a3; pb=b3; }
  d = sqrt(d);
  double dist = (2.0 * hair_radius - d) / (2.0 * hair_radius);
  if (dist < 0.0) { return; }
  vec3 v = (pb - pa).normalize();
  double Fmag = 10.0;
  vec3 F = (Fmag * dist) * v;
  a.accel[i] -= F;
  a.accel[i+1] -= F;
  b.accel[j] += F;
  b.accel[j+1] += F;
}

/* Collision response function for two hairs colliding. */
void hair_hair_collide(Hair &a, Hair &b, double hair_radius) {
  /* Define an interaction distance as four times the hair radius.
     Compare vertices on hair a and b against the interaction
     distance squared as a quick test to see if more complicated
     collision detection is necessary. */
  double d2 = 4.0 * 4.0 * hair_radius * hair_radius;
  for (int i = 0; i < (int) (a.pos.size() - 1); i++) {
    for (int j = 0; j < (int) (b.pos.size() - 1); j++) {
      vec3 delta = a.pos[i] - b.pos[j];
      double dsquared = delta.x*delta.x+delta.y*delta.y+delta.z*delta.z;
      if (dsquared < d2) {
        segment_segment_collide(a, b, i, j, hair_radius);
      }
    }
  }
}

/* Compute pseudoinverse of 3x2 matrix with columns col1 and col2.
   Place the pseudoinverse rows in row1 and row2.
   The pseudoinverse of A is defined as pinv(A) = inv(A'*A)*(A'). */
void pseudoinverse_3by2(vec3 col1, vec3 col2, vec3 &row1, vec3 &row2) {
  double ApA11, ApA12, ApA22;               /* (A'*A). */
  ApA11 = col1 * col1;
  ApA12 = col1 * col2;
  ApA22 = col2 * col2;
  double inv_det = 1.0 / (ApA11*ApA22-ApA12*ApA12);
  double Ainv11 =  ApA22 * inv_det;         /* inv(A'*A). */
  double Ainv12 = -ApA12 * inv_det;
  double Ainv22 =  ApA11 * inv_det;
  row1 = vec3(Ainv11 * col1.x + Ainv12 * col2.x,
              Ainv11 * col1.y + Ainv12 * col2.y,
              Ainv11 * col1.z + Ainv12 * col2.z);
  row2 = vec3(Ainv12 * col1.x + Ainv22 * col2.x,
              Ainv12 * col1.y + Ainv22 * col2.y,
              Ainv12 * col1.z + Ainv22 * col2.z);
}

/* Precomputes some information for a given quad in a "layer" of hair.
   In the constructor we are given the four vertices of the quad:

         a ----- b
         |       |
         c ----- d
*/
class LayerQuadInfo { public:
  vec3 p0;     /* Origin of quad. */
  vec3 v1;     /* First edge vector,  p0 + v1 gives a quad vertex. */
  vec3 v2;     /* Second edge vector, p0 + v2 gives a quad vertex. */
  vec3 N;      /* Unit normal vector. */
  vec3 pinv_row1, pinv_row2;    /* Rows of pseudoinverse. */
  LayerQuadInfo(vec3 a, vec3 b, vec3 c, vec3 d) {
    p0 = a;
    v1 = b - a;
    v2 = c - a;
    N = v1.cross(v2).normalize();
    pseudoinverse_3by2(v1, v2, pinv_row1, pinv_row2);
  }
};

/* Collision response for hair vertex h.pos[j] colliding with hair layer
   quad formed between vertices a[i], a[i+1], b[i], and b[i+1].  Here
   d gives the shortest distance (approximate) between the vertex and
   the quad, and quad_to_pt_vec gives the vector (approximate) from the
   (point on the quad closest to the vertex) to (the vertex). */
void vertex_quad_collide(Hair &h, Hair &a, Hair &b, int i, int j,
                         double hair_radius, double d, vec3 quad_to_pt_vec) {
  double dist = (2.0 * hair_radius - d) / (2.0 * hair_radius);
  if (dist < 0.0) { return; }
  vec3 v = quad_to_pt_vec.normalize();
  double Fmag = 10.0;
  vec3 F = (Fmag * dist) * v;

  /* Apply full force to hair vertex.  Distribute opposing force
     over all four quad vertices, dividing its magnitude by four. */
  vec3 F_frac = F * 0.25;
  a.accel[i]   -= F_frac;
  a.accel[i+1] -= F_frac;
  b.accel[i]   -= F_frac;
  b.accel[i+1] -= F_frac;
  h.accel[j]   += F;
}

/* Collide hair h with layer formed by hairs a-b.
   The info pointer gives a list of precomputed quad information such
   that info[i] corresponds to the LayerQuadInfo constructor called
   on a.pos[i], a.pos[i+1], b.pos[i], b.pos[i+1]. */
void hair_layer_collide(Hair &h, Hair &a, Hair &b, double hair_radius, LayerQuadInfo *info) {
  /* Layer-hair distance of interaction is 2*hair_radius. */
  double d2 = 4.0 * hair_radius * hair_radius;

  /* A higher threshold distance (squared), used as a quick test to
     see if there is (probably) no collision. */
  double d3 = 16.0*d2;

  /* Loop over quads in the layer a-b. */
  for (int i = 0; i < (int) (a.pos.size() - 1); i++) {
    LayerQuadInfo *cur = &info[i];

    /* Loop over segments in the hair h. */
    for (int j = 0; j < (int) h.pos.size(); j++) {
      if (lengthSquared(h.pos[j] - cur->p0) > d3) {
        continue;
      }
      vec3 orig_delta = h.pos[j] - cur->p0;
      /* Subtract off normal component. */
      vec3 delta = orig_delta - (orig_delta * cur->N) * cur->N;
      /* Write remaining vector as u*v1 + v*v2; use pseudoinverse. */
      double u = cur->pinv_row1 * delta;
      double v = cur->pinv_row2 * delta;
      /* Clamp coordinates. */
      if (u < 0.0) { u = 0.0; }
      else if (u > 1.0) { u = 1.0; }
      if (v < 0.0) { v = 0.0; }
      else if (v > 1.0) { v = 1.0; }
      /* Recalculate quad-point distance. */
      vec3 quad_to_pt_vec = orig_delta - (u*cur->v1+v*cur->v2);
      double dsquared = lengthSquared(quad_to_pt_vec);
      if (dsquared < d2) {
        vertex_quad_collide(h, a, b, i, j, hair_radius, sqrt(dsquared),
                            quad_to_pt_vec);
      }
    }
  }
}

/* A rectangular grid of hairs mapped onto a sphere. */
class HairGrid: public Object { public:
  vector<vector<Hair *> > hairs;  /* Rectangular grid of hairs. */
  int nx, ny;                     /* Size of grid. */
  double r;                       /* Radius of sphere. */
  int nodes;                      /* Number of joints per hair. */

  double twist_start;             /* Time when head shake started. */
  int key_twist;                  /* True iff "shake no" key pressed. */
  int key_nod;                    /* True iff "nod yes" key pressed. */
  double twist_angle;             /* Current nod or shake angle. */
  bool twist_is_nod;              /* True iff nodding (else, shaking no). */

  double xmin, ymin, xmax, ymax;  /* Where hair rectangle starts and
                                     ends in normalized (u, v) coords. */

  HairGrid(int nx_=12, int ny_=6, double r_=0.5, double xmin_=0.0, double xmax_=0.5, double ymin_=0.0, double ymax_=0.5,
           double hairlen=1.0, int nodes_=20, vec3 g=vec3(0,0,-9.8), int max_constrain_iters=20) {
    nx = nx_;
    ny = ny_;
    r = r_;
    nodes = nodes_;
    twist_start = -1000.0;
    twist_angle = 0.0;
    key_twist = 0;
    key_nod = 0;
    twist_is_nod = false;
    xmin = xmin_;
    ymin = ymin_;
    xmax = xmax_;
    ymax = ymax_;

    for (int i = 0; i < ny; i++) {
      double theta = (ymin+(ymax-ymin)*(i+1)*1.0/ny) * M_PI;
      vector<Hair *> row;
      for (int j = 0; j < nx; j++) {
        double phi = (xmin+(xmax-xmin)*j*1.0/nx) * 2.0 * M_PI;
        vec3 N = vec3(cos(phi)*sin(theta), sin(phi)*sin(theta), cos(theta));
        row.push_back(new Hair(make_linear_pos_list(N*r, N*(r+hairlen), nodes), g, max_constrain_iters));
      }
      hairs.push_back(row);
    }
  }

  virtual ~HairGrid() {
    for (int i = 0; i < ny; i++) {
      for (int j = 0; j < nx; j++) {
        delete hairs[i][j];
      }
    }
  }

  /* Move hairs to account for nodding "yes" or shaking "no."
     Angles are in radians. */
  void twist(double rot_phi, double rot_theta) {
    for (int i = 0; i < ny; i++) {
      double theta = (ymin+(ymax-ymin)*(i+1)*1.0/ny) * M_PI;
      for (int j = 0; j < nx; j++) {
        double phi = (xmin+(xmax-xmin)*j*1.0/nx) * 2.0 * M_PI + rot_phi;
        mat3 R = mat3().setRotation(rot_theta, vec3(1,0,0));
        vec3 N = R*vec3(cos(phi)*sin(theta), sin(phi)*sin(theta), cos(theta));
        hairs[i][j]->pos[0] = N*r;
      }
    }
  }

  /* Update twisting/nodding motion of head based on user input. */
  void do_twist_anim() {
    double twist_duration = 1.5;

    int prev_key_twist = key_twist;
    key_twist = key[KEY_N];
    if (key_twist && !prev_key_twist) {
      twist_start = world.t;
      twist_is_nod = false;
    }

    int prev_key_nod = key_nod;
    key_nod = key[KEY_Y];
    if (key_nod && !prev_key_nod) {
      twist_start = world.t;
      twist_is_nod = true;
    }

    double trel = (world.t - twist_start) / twist_duration;
    if (trel > 0.0 && trel < 1.0) {
      double f = sin(2*M_PI*trel);
      twist_angle = f * 60.0*M_PI/180.0;
      twist(twist_is_nod ? 0.0: twist_angle, twist_is_nod ? twist_angle: 0.0);
    }
  }

  /* Update hair grid. */
  virtual void update() {
    /* Bounding boxes for whole hairs.  A 2D array flattened to 1D. */
    vector<AABB> hair_box;

    /* Bounding boxes for each "layer:" a single layer runs along two
       adjacent hairs from root to tip. A 2D array flattened to 1D. */
    vector<AABB> horiz_layer;

    /* Information for each quad within each layer.  A 3D array
       flattened to 1D. */
    LayerQuadInfo *horiz_quad_info = (LayerQuadInfo *) malloc(sizeof(LayerQuadInfo)*(nodes-1)*(nx-1)*ny);

    do_twist_anim();

    double hair_radius = 0.9 * 0.5 * 0.05;
    int i, j, k;
    for (i = 0; i < ny; i++) {
      for (j = 0; j < nx; j++) {
        /* Update individual hair physics. */
        hairs[i][j]->update();

        /* Hair-hair interactions: add hair strand AABB to list. */
        hair_box.push_back(hairs[i][j]->get_box());
      }
    }

    /* Effective thickness of each layer. */
    double layer_radius = hair_radius * 2.0;

    /* Hair-hair interactions: add horizontal layer AABBs to list.
       We do not make "vertical" layers, because as the authors note,
       the horizontal layers alone work for simple hairstyles. */
    for (i = 0; i < ny; i++) {
      for (j = 0; j < nx; j++) {
        if (j < nx - 1) {
          AABB layer_box = box_box_union(hair_box[i*nx+j],
                                         hair_box[i*nx+j+1]);
          horiz_layer.push_back(extrude_box(layer_box, layer_radius));

          /* For each quad on the layer, precompute some information. */
          Hair *h1 = hairs[i][j], *h2 = hairs[i][j+1];
          int offset = i*(nx-1)+j;
          for (k = 0; k < (nodes - 1); k++) {
            horiz_quad_info[offset*(nodes-1)+k] = LayerQuadInfo(h1->pos[k], h1->pos[k+1], h2->pos[k], h2->pos[k+1]);
          }
        }
      }
    }

    /* Extrude out hair AABBs by hair radius. */
    for (i = 0; i < (int) hair_box.size(); i++) {
      hair_box[i] = extrude_box(hair_box[i], hair_radius);
    }

    /* Collide hair bounding boxes with themselves via broadphase 1D
       sort algorithm.  This gives us hair-hair pairs for collision. */
    Precollider1D pre_hair(hair_box);
    vector<pair<int, int> > pairs;
    broadphase_1d(pre_hair, pairs);

    /* Collide hair bounding boxes with horizontal layer bounding boxes
       via 1D sort.  This gives us hair-layer pairs for collision. */
    Precollider1D pre_horiz_layer(horiz_layer);
    vector<pair<int, int> > horiz_pairs;
    broadphase_1d(pre_hair, pre_horiz_layer, horiz_pairs);

    int k1, k2, i1, i2, j1, j2;

    /* Respond to hair-layer collisions. */

    /* The hair-layer collision response functions assume that the
       layer and the hair have equal radii -- so compute the
       appropriate radius based on the actual hair and layer radii. */
    double interact_radius = (hair_radius + layer_radius) * 0.5;

    /* Respond to horizontal layer - vertex collisions. */
    for (i = 0; i < (int) horiz_pairs.size(); i++) {
      k1 = horiz_pairs[i].first;
      k2 = horiz_pairs[i].second;
      i1 = k1/nx;     j1 = k1%nx;
      i2 = k2/(nx-1); j2 = k2%(nx-1);
      if (i1 != i2) {
        LayerQuadInfo *info = &horiz_quad_info[(i2*(nx-1)+j2)*(nodes-1)];
        hair_layer_collide(*hairs[i1][j1], *hairs[i2][j2], *hairs[i2][j2+1], interact_radius, info);
      }
    }

    /* Respond to hair-hair segment collisions. */
    for (i = 0; i < (int) pairs.size(); i++) {
      k1 = pairs[i].first;
      k2 = pairs[i].second;
      i1 = k1/nx; j1 = k1%nx;
      i2 = k2/nx; j2 = k2%nx;
      hair_hair_collide(*hairs[i1][j1], *hairs[i2][j2], hair_radius);
    }

    free((void *) horiz_quad_info);
  }

  virtual void render() {
    int i, j, k;
    if (key[KEY_M]) {
      /* Coarse mesh rendering. */
      for (i = 0; i < ny; i++) {
        for (j = 0; j < nx; j++) {
          hairs[i][j]->render();
        }
      }
    } else {
      /* Interpolated fine rendering. */
      int mx = 5, my = 5;   /* Hairs per grid "cell" on scalp. */
      glLineWidth(5.0);
      for (i = 0; i < ny-1; i++) {
        for (j = 0; j < nx-1; j++) {
          for (int xi = 0; xi < mx; xi++) {
            for (int yi = 0; yi < my; yi++) {
              glBegin(GL_LINE_STRIP);
              for (k = 0; k < nodes; k++) {
                /* Change color based on some wavy cos() functions. */
                double yreal = (i+yi/my)/ny, xreal = (j+xi/mx)/nx;
                double lum = cos((yreal*5+xreal*9.49)*5+0.1*k)*0.5+0.5;
                glColor3f(lum, lum, 0.0);

                /* Offset x and y interpolations to add nonuniformity. */
                double arg = xi+yi*2.1239+k;
                double xt = (xi+sin(arg)*0.2) / mx, yt = (yi+cos(arg)*0.2) / my;
                vec3 p1 = hairs[i  ][j  ]->pos[k];
                vec3 p2 = hairs[i  ][j+1]->pos[k];
                vec3 p3 = hairs[i+1][j  ]->pos[k];
                vec3 p4 = hairs[i+1][j+1]->pos[k];
                vec3 pr1 = p1 + (p2 - p1) * xt;
                vec3 pr2 = p3 + (p4 - p3) * xt;
                vec3 p = pr1 + (pr2 - pr1) * yt;

                /* Move outside sphere. */
                if (p.length() < r) {
                  p = p.normalize() * r;
                }
                glVertex3f(p.x, p.y, p.z);
              }
              glEnd();
            }
          }
        }
      }
    }
  }
};

HairGrid *grid = NULL;

/* Render the head, with nodding/shaking as needed. */
void Sphere::render() {
  glEnable(GL_DEPTH_TEST);
  glPushMatrix();
  glTranslatef(pos.x, pos.y, pos.z);
  if (grid != NULL) {
    if (grid->twist_is_nod) {
      glRotatef(180.0/M_PI*grid->twist_angle, 1.0, 0.0, 0.0);
    } else {
      glRotatef(180.0/M_PI*grid->twist_angle, 0.0, 0.0, 1.0);
    }
  }
  render_polymesh(mesh);
  glPopMatrix();
}

/* Helper object, used to reset the scene. */
class NewSceneCreator: public Object { public:
  int current_key_space;
  NewSceneCreator() {
    current_key_space = 0;
  }
  virtual void update() {
    int prev_key_space = current_key_space;
    current_key_space = key[KEY_SPACE];
    if (current_key_space && !prev_key_space) {
      create_new_objects();
    }
  }
};

/* Create new global object variables. */
void create_new_objects() {
  if (grid != NULL) {
    grid->remove();
  }
  if (sphere != NULL) {
    sphere->remove();
  }

  grid = new HairGrid();
  sphere = new Sphere();
  world.add(sphere);
  world.add(grid);
}

void init_scene(int argc, char *argv[]) {
  create_new_objects();
  world.add(new Camera());
  world.add(new BeginScene());
  world.add(new EndScene());
  world.add(new NewSceneCreator());
}

void directions() {
  printf("Hair simulation.\n"
         "Connelly Barnes 2006.\n\n"
         "Controls:\n"
         "  Arrow keys       - Move camera in XY plane (hold Alt to sidestep).\n"
         "  Pageup/Pagedown  - Move up/down.\n"
         "  G                - Turn off gravity.\n"
         "  Y                - Nod 'yes.'\n"
         "  N                - Shake head 'no.'\n"
         "  W                - Fake wind effect.\n"
         "  M                - View coarse hair mesh.\n"
         "  Spacebar         - Reset hair.\n");
}

int main(int argc, char *argv[]) {
  directions();
  alutil_init_gfx_mode();
  alutil_init_perspective();

  init_scene(argc, argv);

  /* Main loop. */
  while (!key[KEY_ESC]) {
    world.update();
    world.render();
  }

  return 0;
}

/* Allegro preprocessor magic to make platform specific main function. */
END_OF_MAIN();
