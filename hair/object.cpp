
/* ----------------------------------------------------------------
   object.cpp: Simple scene objects and an engine.
   ---------------------------------------------------------------- */

#include "object.h"

Object::Object() {
  mass = 0.0;
  remove_flag = false;
}

void Object::update() {
}

void Object::render() {
}

void Object::apply_uniform_force(Object *other, vec3 F) {
}

void Object::remove() {
  remove_flag = true;
}

bool Object::needs_removal() {
  return remove_flag;
}

double Object::update_priority() {
  return 0.0;
}

double Object::render_priority() {
  return 0.0;
}

Object::~Object() {
}

void Object::list_all_objects(vector<Object *> &L) {
  L.push_back(this);
}

vector<Object *> Object::all_objects() {
  vector<Object *> ans;
  list_all_objects(ans);
  return ans;
}

void Object::list_clip_polys(vec3 box_min, vec3 box_max, polymesh &mesh) {
}

polymesh Object::clip_polys(vec3 box_min, vec3 box_max) {
  polymesh mesh;
  list_clip_polys(box_min, box_max, mesh);
  return mesh;
}

/* Function used to sort by update priority. */
class UpdatePriorityComparator { public:
  bool operator() (Object *a, Object *b) {
    return a->update_priority() < b->update_priority();
  }
};

/* Function used to sort by render priority. */
class RenderPriorityComparator { public:
  bool operator() (Object *a, Object *b) {
    return a->render_priority() < b->render_priority();
  }
};

void ContainerObject::clear() {
  children.clear();
}

int ContainerObject::size() {
  return children.size();
}

vector<Object *> ContainerObject::copy_children() {
  vector<Object *> copy;
  for_each(set<Object *>, q, children) {
    copy.push_back(*q);
  }
  return copy;
}

void ContainerObject::add(Object *x) {
  children.insert(x);
}

void ContainerObject::render() {
  vector<Object *> copy = copy_children();
  sort(copy.begin(), copy.end(), RenderPriorityComparator());
  for_each(vector<Object *>, p, copy) {
    (*p)->render();
  }
}

void ContainerObject::update() {
  vector<Object *> copy = copy_children();
  sort(copy.begin(), copy.end(), UpdatePriorityComparator());
  for_each(vector<Object *>, p, copy) {
    (*p)->update();
  }
  copy = copy_children();
  children.clear();
  for_each(vector<Object *>, q, copy) {
    if ((*q)->needs_removal()) {
      delete *q;
    } else {
      children.insert(*q);
    }
  }
}

ContainerObject::~ContainerObject() {
  vector<Object *> copy = copy_children();
  for_each(vector<Object *>, p, copy) {
    delete *p;
  }
}

void ContainerObject::list_all_objects(vector<Object *> &L) {
  vector<Object *> copy = copy_children();
  L.push_back(this);
  for_each(vector<Object *>, p, copy) {
    (*p)->list_all_objects(L);
  }
}

void ContainerObject::list_clip_polys(vec3 box_min, vec3 box_max, polymesh &mesh) {
  vector<Object *> copy = copy_children();
  for_each(vector<Object *>, p, copy) {
    (*p)->list_clip_polys(box_min, box_max, mesh);
  }
}

World::World(double dt_) {
  t = 0.0;
  dt = dt_;
  next_wall_time = 0.0;
  render_framerate = 0.0;
  update_framerate = 0.0;
}

void World::update() {
  double max_dt = 1.0 / 20.0;   /* Give a minimum rendering framerate. */
  double cur_dt = 0.0;
  for (;;) {
    double wall_time = alutil_clock();
    if (wall_time < next_wall_time) {
      break;
    }
    t += dt;
    cur_dt += dt;
    next_wall_time += dt;
    ContainerObject::update();
    double end_time = alutil_clock();
    double T = end_time - wall_time;
    update_framerate = T != 0.0 ? 1.0 / T: 0.0;
    if (cur_dt > max_dt) { break; }
  }
}

void World::render() {
  double start_time = alutil_clock();
  ContainerObject::render();
  double end_time = alutil_clock();
  double T = end_time - start_time;
  render_framerate = T != 0.0 ? 1.0 / T: 0.0;
}

World world;

Camera::Camera(vec3 pos_, double heading_, double pitch_, double roll_) {
  pos = pos_;
  heading = heading_;
  pitch = pitch_;
  roll = roll_;
}

mat4 Camera::camera_matrix(bool do_translate) {
  mat4 rest = (mat4::rotation(heading, vec3(0,0,1)) *
               mat4::rotation(pitch,   vec3(1,0,0)) * 
               mat4::rotation(roll,    vec3(0,1,0)));
  if (do_translate) {
    return mat4::translation(vec3(pos)) * rest;
  } else {
    return rest;
  }
}

/* Forward vector in world coords. */
vec3 Camera::forward() {
  return camera_matrix(false) * vec3(0,1,0);
}

/* Right vector in world coords. */
vec3 Camera::right() {
  return camera_matrix(false) * vec3(1,0,0);
}

/* Up vector in world coords. */
vec3 Camera::up() {
  return camera_matrix(false) * vec3(0,0,1);
}

/* Set your GL_MODELVIEW matrix to this to render the scene from the camera. */
mat4 Camera::full_matrix() {
  mat4 flip = mat4(1, 0, 0, 0,
                   0, 0, 1, 0,
                   0,-1, 0, 0,
                   0, 0, 0, 1);
  return (flip * camera_matrix().inverse());
}

void Camera::update() {
  double speed = 10.0 * 0.5, rspeed = 1.0;
  int sidestep = key[KEY_ALT];
  if (key[KEY_LEFT]) {
    if (sidestep) {
      pos -= speed * world.dt * right();
    } else {
      heading += rspeed * world.dt;
    }
  }
  if (key[KEY_RIGHT]) {
    if (sidestep) {
      pos += speed * world.dt * right();
    } else {
      heading -= rspeed * world.dt;
    }
  }
  if (key[KEY_PGUP]) {
    pos += speed * world.dt * up();
  }
  if (key[KEY_PGDN]) {
    pos -= speed * world.dt * up();
  }
  if (key[KEY_UP]) {
    pos += speed * world.dt * forward();
  }
  if (key[KEY_DOWN]) {
    pos -= speed * world.dt * forward();
  }
}

double Camera::render_priority() {
  return RENDER_PRIORITY_CAMERA;
}

void Camera::render() {
  double M[16];
  glMatrixMode(GL_MODELVIEW);
  full_matrix().toList(M, false);
  glLoadMatrixd(M);
}

void BeginScene::render() {
  glDepthMask(GL_TRUE);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

double BeginScene::render_priority() {
  return RENDER_PRIORITY_BEGINSCENE;
}

void EndScene::render() {
  glFlush();
  allegro_gl_flip();
}

double EndScene::render_priority() {
  return RENDER_PRIORITY_ENDSCENE;
}
