
/* ---------------------------------------------------------------
   constraint.h: Constraint macros used in relaxation for physics.
   --------------------------------------------------------------- */

#ifndef _constraint_h
#define _constraint_h

/* Move both p1 and p2 so ||p2 - p1|| = dest_length. */
#define CONSTRAIN_MOVE_MOVE(p1, p2, dest_length)             \
{                                                            \
  vec3 _delta = (p2) - (p1);                                 \
  double _distance = _delta.x*_delta.x+_delta.y*_delta.y+_delta.z*_delta.z;                        \
  _distance = sqrt(_distance);                               \
  _delta *= 0.5 * (((dest_length) - _distance) / _distance); \
  (p1) -= _delta;                                            \
  (p2) += _delta;                                            \
}

/* Move only pmoving so ||pmoving - pfixed|| = dest_length. */
#define CONSTRAIN_FIX_MOVE(pfixed, pmoving, dest_length)     \
(pmoving) = (pfixed) + ((pmoving) - (pfixed)).normalize() * (dest_length);

/* Move p2, p3 so shortest_angle(p2-p1,p3-p1) = theta.
   Triangle edge lengths are maintained. */
void constrain_angle3(vec3 p1, vec3 &p2, vec3 &p3, double theta) {
  vec3 v1 = pseudonormalize(p2-p1);
  vec3 v2 = pseudonormalize(p3-p1);
  vec3 mid = pseudonormalize(v1+v2);
  if (mid.x < 0.0) { mid = -mid; }
  vec3 out = vec3(0,1,0);//mid.cross(v2);
  vec3 up = pseudonormalize(out.cross(mid));
  double c = cos(theta*0.5), s = sin(theta*0.5);
  p2 = p1 + (mid*c + up*s) * (p2-p1).length();
  p3 = p1 + (mid*c - up*s) * (p3-p1).length();
}

#endif
