
/* ----------------------------------------------------------------
   alutil.h: Create GL context, timer, keyboard input via AllegGL.
   ---------------------------------------------------------------- */

#ifndef _alutil_h
#define _alutil_h

#include <alleggl.h>
#include <gl/glu.h>
#include "global.h"

/* Accurate wall clock in seconds. */
double alutil_clock();

/* Set up a GL video mode. */
void alutil_init_gfx_mode(int width=1024, int height=768, int depth=32, int zdepth=24, bool window=true);

/* Set up perspective matrix. */
void alutil_init_perspective(double fov=45.0, double zmin=0.001, double zmax=1000.0);

#endif
