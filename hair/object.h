
/* ----------------------------------------------------------------
   object.h: Simple scene objects and an engine (called World).
   ---------------------------------------------------------------- */

#ifndef _object_h
#define _object_h

#include "global.h"

/* Objects are sorted ascending by priority before their render()
   or update() methods are called.  Some predefined priorities: */

#define RENDER_PRIORITY_BEGINSCENE -1000
#define RENDER_PRIORITY_CAMERA     -100
#define RENDER_PRIORITY_NORMAL     0
#define RENDER_PRIORITY_TRANS      1
#define RENDER_PRIORITY_ENDSCENE   1000

/* Object: A generic "object" in the 3D world. */
class Object {
  private:
  bool remove_flag;

  public:
  double mass;

  Object();

  /* Update position, velocity, respond to collisions, etc. */
  virtual void update();

  /* Draw to screen. */
  virtual void render();

  /* Apply uniform force F; other is the object applying the force. */
  virtual void apply_uniform_force(Object *other, vec3 F);

  /* Flag us for removal at end of next World::update() loop. */
  virtual void remove();

  /* Do we need to be removed? */
  virtual bool needs_removal();

  /* Objects are sorted by update priority prior to calling update(). */
  virtual double update_priority();

  /* Objects are sorted by render priority prior to calling render(). */
  virtual double render_priority();

  virtual ~Object();

  /* Append this and all child objects to the given list. */
  virtual void list_all_objects(vector<Object *> &L);

  /* Same as list_all_objects(), but returns the list instead. */
  vector<Object *> all_objects();

  /* List static polygons that a moving object should clip against.
     Here box_min, box_max are the coords of an AABB that the moving
     object is contained in.  Use a minimal AABB to get fewer clip polygons.
     Note that polymesh is just a typedef for vector<vector<vec3> >, i.e.
     a list of polygons. */
  virtual void list_clip_polys(vec3 box_min, vec3 box_max, polymesh &mesh);

  /* Same as list_clip_polys(), but returns the polymesh instead. */
  virtual polymesh clip_polys(vec3 box_min, vec3 box_max);
};

/* Generic "container object" which can contain child objects. */
class ContainerObject: public Object {
  private:
  set<Object *> children;

  public:

  /* Remove all child objects. */
  void clear();

  /* Get number of child objects. */
  int size();

  /* Get a list copy of all child objects, in no particular order. */
  vector<Object *> copy_children();

  /* Add a child object.  The container will call delete on the child
     object when it is finished with it. */
  void add(Object *x);

  /* Render the container and all child objects. */
  virtual void render();

  /* Update the container and all child objects. */
  virtual void update();

  virtual ~ContainerObject();

  virtual void list_all_objects(vector<Object *> &L);

  virtual void list_clip_polys(vec3 box_min, vec3 box_max, polymesh &mesh);
};

/* Root container for all objects, also holds timing information. */
class World: public ContainerObject {
  public:
  double t;                  /* Current time, secs. */
  double dt;                 /* Timestep for integrator, secs.  This is a fixed rate. */
  double next_wall_time;     /* Next wall time that we will move t forward. */
  double render_framerate;   /* Estimated framerate if we just did rendering. */
  double update_framerate;   /* Estimated framerate if we just did updating. */

  World(double dt_=1.0/40.0);
  virtual void render();
  virtual void update();
};

extern World world;

/* Camera: A typical 3D shooter style camera. */
class Camera: public Object {
  public:
  vec3 pos;
  double heading;
  double pitch;
  double roll;

  Camera(vec3 pos_=vec3(0,0,0), double heading_=0.0, double pitch_=0.0, double roll_=0.0);

  mat4 camera_matrix(bool do_translate=true);

  /* Forward vector in world coords. */
  vec3 forward();

  /* Right vector in world coords. */
  vec3 right();

  /* Up vector in world coords. */
  vec3 up();

  /* Set your GL_MODELVIEW matrix to this to render the scene from the camera. */
  mat4 full_matrix();

  /* Move camera in response to user input. */
  virtual void update();

  virtual double render_priority();

  /* Rendering the camera sets the GL_MODELVIEW matrix to the camera matrix. */
  virtual void render();
};


/* BeginScene: Helper object used to render start of scene. */
class BeginScene: public Object { public:
  virtual void render();
  virtual double render_priority();
};

/* EndScene: Helper object used to render end of scene. */
class EndScene: public Object { public:
  virtual void render();
  virtual double render_priority();
};

#endif
