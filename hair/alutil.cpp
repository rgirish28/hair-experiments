
/* ----------------------------------------------------------------
   alutil.cpp: Create GL context, timer, keyboard input via AllegGL.
   ---------------------------------------------------------------- */

#include "alutil.h"

#define ALUTIL_CLOCKRATE 1000

volatile int __elapsed_time = 0;
void __inc_elapsed_time() { __elapsed_time++; }
END_OF_FUNCTION(__inc_elapsed_time);

double alutil_clock() {
  return __elapsed_time * (1.0/ALUTIL_CLOCKRATE);
}

void alutil_init_gfx_mode(int width, int height, int depth, int zdepth, bool window) {
  allegro_init();
  install_allegro_gl();

  install_keyboard();
  install_mouse();
  install_timer();
  LOCK_VARIABLE(__elapsed_time);
  LOCK_FUNCTION(__inc_elapsed_time);
  install_int_ex(__inc_elapsed_time, BPS_TO_TIMER(ALUTIL_CLOCKRATE));

  allegro_gl_clear_settings();
  allegro_gl_set(AGL_RENDERMETHOD, 1);
  allegro_gl_set(AGL_Z_DEPTH, zdepth);
  allegro_gl_set(AGL_COLOR_DEPTH, depth);
  allegro_gl_set(AGL_SUGGEST, AGL_Z_DEPTH | AGL_COLOR_DEPTH | AGL_RENDERMETHOD);
  int mode = window ? GFX_OPENGL_WINDOWED: GFX_OPENGL_FULLSCREEN;
  if (set_gfx_mode(mode, width, height, 0, 0)) {
    allegro_message("Error setting %dx%d video mode\n", width, height);
    exit(1);
  }
  allegro_gl_set_texture_format(GL_RGB8);
//  glViewport(0,0,width,height);
}

void alutil_check_types() {
  if (sizeof(uint8) != 1 || sizeof(uint16) != 2 || sizeof(uint32) != 4 ||
      sizeof( int8) != 1 || sizeof( int16) != 2 || sizeof( int32) != 4) {
    allegro_message("Error: uint8/16/32 or int8/16/32 in alutil.h has wrong size.\n");
    exit(1);
  }
}

void alutil_init_perspective(double fov, double zmin, double zmax) {
  glDepthFunc(GL_LESS);
  glClearColor(0.0, 0.0, 0.0, 0.0);

  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluPerspective(fov, 1.0 * screen->w/screen->h, zmin, zmax);
  glMatrixMode(GL_MODELVIEW);
}
