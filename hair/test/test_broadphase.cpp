
/* ---------------------------------------------------------------
   test_broadphase.cpp: Unit tests for broadphase.cpp.
   --------------------------------------------------------------- */

#include "../broadphase.h"
#include <stdlib.h>
#include <time.h>

/* Randoms in [0, 1) and [-1, 1), well distributed. */
#undef crandom
#undef random
#define random() ((rand() ^ (rand()<<16))/(INT_MAX+1.0))
#define crandom() (random()*2.0-1.0)

AABB random_box(double max_size) {
  vec3 center = vec3(crandom(), crandom(), crandom());
  vec3 size = vec3(random(), random(), random()) * max_size;
  return AABB(center - size*0.5, center + size*0.5);
}

typedef set<pair<int, int> > set_pair;

void test_broadphase_1d_AB(int n) {
  vector<AABB> A, B;
  double size = 0.125;
  int i;
  for (i = 0; i < n; i++) {
    A.push_back(random_box(size));
    B.push_back(random_box(size));
  }
  AABB box(vec3(-1-size*0.5,-1-size*0.5,-1-size*0.5),
           vec3( 1+size*0.5, 1+size*0.5, 1+size*0.5));
  PrecolliderOctree octA(A, box), octB(B, box);
  vector<pair<int, int> > c1;
  broadphase_naive(A, B, c1);
  vector<pair<int, int> > c2;
  broadphase_1d(A, B, c2);
  vector<pair<int, int> > c3;
  broadphase_octree(octA, octB, c3);
  sort(c1.begin(), c1.end());
  sort(c2.begin(), c2.end());
  sort(c3.begin(), c3.end());
  if (c1 != c2 || c2 != c3) {
    printf("Not equal %d %d %d\n", c1.size(), c2.size(), c3.size()); fflush(stdout); exit(1);
  }
}

void test_broadphase_1d_A(int n) {
  vector<AABB> A, B;
  double size = 0.125;
  int i;
  for (i = 0; i < n; i++) {
    A.push_back(random_box(size));
    B.push_back(random_box(size));
  }
  vector<pair<int, int> > c1;
  broadphase_naive(A, c1);
  vector<pair<int, int> > c2;
  broadphase_1d(A, c2);
  for (i = 0; i < (int) c2.size(); i++) {
    if (c2[i].first > c2[i].second) {
      c2[i] = pair<int, int>(c2[i].second, c2[i].first);
    }
  }
  sort(c1.begin(), c1.end());
  sort(c2.begin(), c2.end());
  if (c1 != c2) {
    printf("Not equal %d %d\n", c1.size(), c2.size()); fflush(stdout); exit(1);
  }
}

/* Unit test main routine. */
void test() {
  printf("Testing:\n");
  int i;
  for (i = 0; i < 40; i++) {
    test_broadphase_1d_AB(100);
    test_broadphase_1d_AB(1000);
    if (i % 5 == 0) {
      test_broadphase_1d_AB(3000);
    }
  }
  printf("  broadphase_1d(A, B):    OK\n");
  for (i = 0; i < 40; i++) {
    test_broadphase_1d_A(100);
    test_broadphase_1d_A(1000);
    if (i % 5 == 0) {
      test_broadphase_1d_A(3000);
    }
  }
  printf("  broadphase_1d(A):       OK\n\n");
}

void bench_broadphase(int n, int m, int q) {
  vector<AABB> A, B;
  double size = 0.125;
  int i;
  for (i = 0; i < n; i++) {
    A.push_back(random_box(size));
    B.push_back(random_box(size));
  }
  int count = 0;
  clock_t start = clock();
  for (i = 0; i < m; i++) {
    vector<pair<int, int> > c1;
    broadphase_naive(A, B, c1);
    count += (int) c1.size();
  }
  clock_t end = clock();
  double T1 = (end - start) * 1.0 / CLOCKS_PER_SEC / m;

  int m2 = m*q;
  start = clock();
  for (i = 0; i < m2; i++) {
    vector<pair<int, int> > c2;
    broadphase_1d(A, B, c2);
    count += (int) c2.size();
  }
  end = clock();
  double T2 = (end - start) * 1.0 / CLOCKS_PER_SEC / m2;

  start = clock();
  for (i = 0; i < m; i++) {
    AABB box(vec3(-1-size*0.5,-1-size*0.5,-1-size*0.5),
             vec3( 1+size*0.5, 1+size*0.5, 1+size*0.5));
    PrecolliderOctree octA(A, box), octB(B, box);
    vector<pair<int, int> > c3;
    broadphase_octree(octA, octB, c3);
    count += (int) c3.size();
  }
  end = clock();
  double T3 = (end - start) * 1.0 / CLOCKS_PER_SEC / m;

  printf("  n=%d:                   (%d collisions)\n", n, count / (m+m2+m));
  printf("  broadphase_naive/sec:   %f\n", 1.0/T1);
  printf("  broadphase_1d/sec:      %f\n", 1.0/T2);
  printf("  broadphase_octree/sec:  %f\n\n", 1.0/T3);
}

void bench() {
  printf("Benchmarking:\n");
/*
  bench_broadphase(10, 1000000, 1);
  bench_broadphase(100, 10000, 5);
  bench_broadphase(1000, 100, 20);
*/
  bench_broadphase(10, 400000, 1);
  bench_broadphase(100, 40000, 5);
  bench_broadphase(300, 4000, 20);
  bench_broadphase(1000, 400, 20);
}

int main() {
  test();
  bench();
  return 0;
}

END_OF_MAIN();
