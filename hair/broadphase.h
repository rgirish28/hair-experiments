
/* ---------------------------------------------------------------
   broadphase.h: Broadphase collision detection for AABBs.
   --------------------------------------------------------------- */

#ifndef _broadphase_h
#define _broadphase_h

#include "global.h"

/* Axis-Aligned Bounding Box in R^3. */
class AABB { public:
  vec3 min, max;         /* Minimum and maximum coord, respectively. */
  AABB(vec3 min_, vec3 max_);
};

int operator == (AABB a, AABB b);
int operator != (AABB a, AABB b);

/* True iff box a intersects with box b. */
int box_box_intersects(AABB a, AABB b);

/* Return union of two boxes. */
AABB box_box_union(AABB a, AABB b);

/* Return bounding box for quad. */
AABB quad_box(vec3 a, vec3 b, vec3 c, vec3 d);

/* Return box extruded outward by displacement r. */
AABB extrude_box(AABB box, double r);

/* Naive broadphase collision (brute force).
   Adds to ans a list of (i, j) such that A[i] collides with B[j]. */
void broadphase_naive(const vector<AABB> &A, const vector<AABB> &B,
                      vector<pair<int, int> > &ans);

/* Naive broadphase collision (brute force).
   Adds to ans a list of (i, j) such that A[i] collides with A[j].
   If (i, j) is present then (j, i) will not be present. */
void broadphase_naive(const vector<AABB> &A,
                      vector<pair<int, int> > &ans);

/* Precomputes some collision information for broadphase_1d(). */
class Precollider1D { public:
  const vector<AABB> &L;
  int *indices;    /* List of indices sorted by minimum x coord. */
  Precollider1D(const vector<AABB> &L_);
  ~Precollider1D();
  /* Do not use copy constructor or assignment operator. */
};

/* Broadphase collision by 1D sort, with precalculation:

     Precollider1D preA(A), preB(B);  // A and B are lists of AABBs.
     broadphase_1d(preA, preB, ans);  // Gets collision index pairs.

   Adds to ans a list of (i, j) such that A[i] collides with B[j]. */
void broadphase_1d(const Precollider1D &A, const Precollider1D &B,
                   vector<pair<int, int> > &ans);

/* Broadphase collision by 1D sort, with precalculation.
   Adds to ans a list of (i, j) such that A[i] collides with A[j].
   If (i, j) is present then (j, i) will not be present. */
void broadphase_1d(const Precollider1D &A,
                   vector<pair<int, int> > &ans);

class PrecolliderOctreeNode;

/* Precomputed octree for broadphase_octree(). */
class PrecolliderOctree { public:
  const vector<AABB> &L;         /* List of objects. */
  AABB box;                      /* Root node box. */
  PrecolliderOctreeNode *root;
  PrecolliderOctree(const vector<AABB> &L_, AABB box_);
  ~PrecolliderOctree();
  /* Do not use copy constructor or assignment operator. */
};


/* Broadphase collision by octree, with precalculation:

     AABB box(vec3(-1,-1,-1), vec3(1,1,1));        // Root node box
     PrecolliderOctree preA(A, box), preB(B, box);
     broadphase_octree(preA, preB);

  Here A and B are lists of AABBs and broadphase_octree() adds to ans a
  list of indices (ai, bi) such that A[ai] collides with B[bi]. */
void broadphase_octree(const PrecolliderOctree &A, const PrecolliderOctree &B,
                       vector<pair<int, int> > &ans);

#endif
